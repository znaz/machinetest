import { useState } from "react";

function App() {
  const [expand, setExpand] = useState(false);
  const [showQuestion, setShowQuestion] = useState(false);
  const [openQuenstion, setOpenQuestion] = useState(false);
  const [showExpand, setShowExpand] = useState(false);
  const [expandQuestion, setExpandQuestion] = useState(true);
  const [expandBox1, setExpandBox1] = useState(false);
  const [expandBox2, setExpandBox2] = useState(false)
  return (
    <div className="flex">
      <aside className="bg-orange-500/80 h-screen w-1/3">&nbsp;</aside>
      <div className="bg-slate-100 w-full pl-16 pt-10">
        {!expand ? (
          <div className="bg-white w-4/5 pb-6 rounded-lg p-4 gap-4 flex justify-between">
            <div className="">
              <span className="text-xs font-medium">STEP 1/3</span>
              <h2 className="text-3xl font-bold mt-3">Cost Identification</h2>
              <p className="text-sm">
                Identify and categorize the various costs influencing margins.
              </p>
            </div>
            <div className="flex flex-col gap-1">
              <span className="text-xs font-medium self-end">
                4 Prompts | 5 mins
              </span>
              <div className="bg-blue-100/50 text-sm font-semibold text-blue-600 flex items-center justify-center px-4 py-6 rounded-xl">
                <button onClick={() => setExpand(true)}>Get Started</button>
                <img src="icons/navigateRight.svg" alt="navigate" />
              </div>
            </div>
          </div>
        ) : (
          <div className="flex flex-col bg-white w-4/5 pb-6 rounded-lg p-4 gap-4 justify-between">
            <div className="flex flex-col">
              <div className="flex mb-1 justify-between items-center">
                <span className="font-medium text-sm">Step 1</span>
                <button
                  onClick={() => setExpand(false)}
                  className="bg-blue-100/50 rounded-full"
                >
                  <img src="/icons/up.svg" alt="minimise" />
                </button>
              </div>
              <h2 className="text-xl mb-2 font-bold">Cost Identification</h2>
              <p className="text-sm mb-4">
                Identify and categorize the various costs influencing margins.
                Costs are usually categorized as variable costs (which increase
                directly with an increase in sales or revenue – for example raw
                material costs for production), semi-variable costs (which
                increase with sales too, but not as linearly. For example –
                manpower costs for some services heavy business) and fixed costs
                (which are not linked to the volume of products/sales/revenue
                directly. For example – headquarter administration costs).
              </p>
              <hr />
            </div>
            <div className="flex items-start ">
              <div>
                <span className="text-xs font-medium text-zinc-400">
                  Question 1 out of 3
                </span>
                <h3 className="text-[18px] font-semibold">
                  What are the primary components of variable & semi-variable
                  costs impacting gross margins?
                </h3>
              </div>
              <button onClick={() => setShowQuestion(!showQuestion)}>
                {!showQuestion ? (
                  <img className="w-8" src="/icons/add.svg" alt="expand" />
                ) : (
                  <img className="w-8" src="/icons/minus.svg" alt="remove" />
                )}
              </button>
            </div>
            {showQuestion && (
              <div>
                <div className="flex gap-4">
                  {!openQuenstion ? (
                    <div
                      className={`flex bg-blue-100/50 items-center rounded-lg  px-2 h-10  justify-center gap-8 `}
                    >
                      <button className="text-black flex items-center ">
                        <img
                          className="w-3 mr-2"
                          src="/images/arrow.png"
                          alt=""
                        />
                        <p className="text-xs font-medium text-blue-700">
                          Need more clarification on the section?
                        </p>
                      </button>
                      <button onClick={() => setOpenQuestion(true)}>
                        <img src="/icons/plus.svg" alt="" />
                      </button>
                    </div>
                  ) : (
                    <div className="w-full py-2 bg-blue-100/50 rounded-lg  px-2 h-64">
                      <div className={`w-full flex justify-between  `}>
                        <div className="">
                          <button className="text-black  flex items-center ">
                            <img
                              className="w-3 mr-2"
                              src="/images/arrow.png"
                              alt=""
                            />
                            <p className="text-xs font-medium text-blue-700">
                              Need more clarification on the section?
                            </p>
                          </button>
                        </div>
                        <button
                          className="self-start"
                          onClick={() => setOpenQuestion(false)}
                        >
                          <img src="/icons/remove.svg" alt="remove" />
                        </button>
                      </div>
                      <div
                        onMouseEnter={() => setShowExpand(true)}
                        onMouseLeave={() => setShowExpand(false)}
                        className={`bg-white mt-2 rounded-full flex flex-col  pr-4 ${
                          expandQuestion ? "w-fit" : "w-full p-4 rounded-md"
                        }`}
                      >
                        <div className="flex justify-between">
                          <div className="flex  p-2 cursor-pointer  gap-4  ">
                            <img
                              className="w-5 self-start"
                              src="/icons/zoom.svg"
                              alt=""
                            />
                            <p className="text-xs">
                              How do I connect this to my business context?
                            </p>
                          </div>
                          <div className="flex items-center mt-3 self-start gap-4">
                            {!expandQuestion && (
                              <img
                                className="w-4"
                                src="/icons/refresh.svg"
                                alt="refresh"
                              />
                            )}
                            {showExpand && (
                              <button
                                onClick={() =>
                                  setExpandQuestion(!expandQuestion)
                                }
                              >
                                <img
                                  className="cursor-pointer "
                                  src="/icons/expand.svg"
                                  alt=""
                                />
                              </button>
                            )}
                          </div>
                        </div>
                        {!expandQuestion && (
                          <div className="flex gap-2">
                            <div className="bg-blue-100 rounded flex flex-col p-2 w-fit h-fit">
                              <div className="flex justify-between items-center ">
                                <img src="/icons/menu.svg" alt="button" />
                                <span className="text-xs text-gray-700">
                                  Add raw materials costs as variable costs
                                </span>
                                <button
                                  onClick={() => setExpandBox1(!expandBox1)}
                                >
                                  <img src="/icons/down.svg" alt="" />
                                </button>
                              </div>
                              <p className={`text-[13px] ml-4 ${expandBox1&& "text-blue-600"}  font-medium`}>
                                Energy and Utility costs are Lorem ipsum dolor
                                sit amet...
                              </p>
                                {expandBox1 && (
                                  <div className="flex mt-2 ml-4 gap-4">
                                    <button className="flex py-1 items-center bg-blue-800 px-2 text-white rounded-full">
                                      <span className="text-xs text-center">ACCEPT</span>
                                      <img className="w-3" src="/icons/downarrow.svg" alt="" />
                                    </button>
                                    <button className="flex items-center ">
                                      <span className="text-xs">DISMISS</span>
                                      <img className="w-3" src="/icons/close.svg" alt="" />
                                    </button>
                                  </div>
                                )}
                            </div>
                            <div className="bg-blue-100 rounded p-2 w-fit h-fit">
                              <div className="flex items-center justify-between ">
                                <img src="/icons/menu.svg" alt="button" />
                                <span className="text-xs text-gray-700">
                                  Add raw materials costs as variable costs
                                </span>
                                <button
                                  onClick={() => setExpandBox2(!expandBox2)}
                                >
                                  <img src="/icons/down.svg" alt="" />
                                </button>
                              </div>
                              <p className={`text-[13px] ml-4 ${expandBox2&& "text-blue-600"}  font-medium`}>
                                Energy and Utility costs are Lorem ipsum dolor
                                sit amet...
                              </p>
                              {expandBox2 && (
                                  <div className="flex mt-2 ml-4 gap-4">
                                    <button className="flex py-1 items-center bg-blue-800 px-2 text-white rounded-full">
                                      <span className="text-xs text-center">ACCEPT</span>
                                      <img className="w-3" src="/icons/downarrow.svg" alt="" />
                                    </button>
                                    <button className="flex items-center ">
                                      <span className="text-xs">DISMISS</span>
                                      <img className="w-3" src="/icons/close.svg" alt="" />
                                    </button>
                                  </div>
                                )}
                            </div>
                          </div>
                        )}
                      </div>
                    </div>
                  )}
                  {!openQuenstion && (
                    <div className="flex bg-blue-100/50 items-center rounded-lg  px-2 h-10  justify-center gap-8">
                      <button className="text-black flex items-center ">
                        <img
                          className="w-3 mr-2"
                          src="/images/arrow.png"
                          alt=""
                        />
                        <p className="text-xs font-medium text-blue-700">
                          Need more clarification on the section?
                        </p>
                      </button>
                      <button>
                        <img src="/icons/plus.svg" alt="" />
                      </button>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
